from random import randint

name = input("Hi! What is your name?\n")
guess_num = 1


for guess in range(5):
    rand_month = randint(1, 12)
    rand_year = randint(1924, 2004)
    print("Guess ", guess_num, " : ", name, "were you born in", rand_month, "/", rand_year, "?")
    answer = input("yes or no? ")

    if (answer == "yes"):
        print("I knew it!")
        exit()
    elif (answer == "no"):
        print("Drat! Lemme try again!")
        guess_num = guess_num + 1

print("I have other things to do. Good bye.")